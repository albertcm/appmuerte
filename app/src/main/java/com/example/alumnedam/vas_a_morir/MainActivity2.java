package com.example.alumnedam.vas_a_morir;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class MainActivity2 extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Calendar unaFecha;
        int numero = 0;
        Random aleatorio;
        aleatorio = new Random();

        unaFecha = Calendar.getInstance();
        unaFecha.set (aleatorio.nextInt(25)+2030, aleatorio.nextInt(12)+1, aleatorio.nextInt(30)+1);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


        String nom = getIntent().getExtras().getString("nombre");
        String lugarNac = getIntent().getExtras().getString("lugarNac");
        TextView tvm = (TextView) findViewById(R.id.tv_muerte);
        String[] muertes = {"Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" a consecuencia de un tiroteo en "+lugarNac+".",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" asesinado por su ex novia a sangre fría en "+lugarNac+".",
                "Señor/a "+nom+" usted tomará la decisión de suicidarse el "+sdf.format(unaFecha.getTime())+", y lo peor de todo es que morirá virgen.",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" a causa de que se le olvidará respirar.",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" de una brutal paliza que le pegará una gamba enfurecida.",
                "Señor/a "+nom+" ES USTED INMORTAL!!! =)",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" atragantado con su propia saliva y todo ocurrirá en "+lugarNac+".",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" y será troceado y comido por un grupo de turistas chinos.",
                "Señor/a "+nom+" usted morirá el "+sdf.format(unaFecha.getTime())+" atropellado por un tren por querer recuperar una moneda de 5 centimos."

        };

        int length = muertes.length;

        for (int i = 0; i < length; i++)
        {
            int rand = (int) (Math.random() * length);

            tvm.setText(muertes[rand]);

        }

    }
        }