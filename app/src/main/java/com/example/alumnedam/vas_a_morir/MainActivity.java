package com.example.alumnedam.vas_a_morir;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Calendar;

import java.util.Date;


public class MainActivity extends AppCompatActivity{
    TextView tv;
    Calendar mCurrentDate;
    int day, month, year;
String nom;
String lugarNac;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        //SEXO SPINNER
        Spinner spinner = (Spinner) findViewById(R.id.sexo);
        String[] datos = new String[] {"Hombre", "Mujer"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos);
        spinner.setAdapter(adapter);

        //PROFESION SPINNER
        Spinner spinnerProfesion = (Spinner) findViewById(R.id.profesion);
        String[] datos2 = new String[] {"Funcionario", "Pringao"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos2);
        spinnerProfesion.setAdapter(adapter2);


        //INTENT ENVIAR
        enviar = (Button) findViewById(R.id.btn_enviar);

        enviar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                EditText etnom = (EditText) findViewById(R.id.editText4);
                nom = etnom.getText().toString();
                etnom.setText(nom);
                EditText etlugar = (EditText) findViewById(R.id.editText2);
                lugarNac = etlugar.getText().toString();
                etlugar.setText(lugarNac);
                Intent i = new Intent(MainActivity.this, MainActivity2.class);
                i.putExtra("nombre", nom);
                i.putExtra("lugarNac", lugarNac);

                startActivity(i);
            }
        });
        //CALENDARIO

        tv = (TextView) findViewById(R.id.textView4);

        mCurrentDate = Calendar.getInstance();
        day = mCurrentDate.get(Calendar.DAY_OF_MONTH);
        month = mCurrentDate.get(Calendar.MONTH);
        year = mCurrentDate.get(Calendar.YEAR);

        month = month+1;

        tv.setText("Cambiar fecha");

        tv.setOnClickListener(new View.OnClickListener(){
@Override
    public void onClick(View v){
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            month = month + 1;
            tv.setText(dayOfMonth + "/" + month + "/" + year);

        }
    }, year, month, day);
    datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
    datePickerDialog.show();
            }
        });
    }

    }



