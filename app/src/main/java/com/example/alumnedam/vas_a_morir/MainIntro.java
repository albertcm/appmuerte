package com.example.alumnedam.vas_a_morir;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by ALBERT on 03/02/2018.
 */

public class MainIntro extends AppCompatActivity {

    Button enviarAMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //INTENT ENVIAR
        enviarAMain = (Button) findViewById(R.id.btn_intro);
        enviarAMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainIntro.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}